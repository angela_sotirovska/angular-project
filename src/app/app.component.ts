import { Component, OnInit } from '@angular/core';
import { Image } from './image';
import { GalleryService } from './services/gallery.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [GalleryService]
})
export class AppComponent implements OnInit {

  images: Image[] = [];

  constructor(private galleryService: GalleryService){}
  ngOnInit(): void {
    this.images=this.galleryService.getImages();
  }

  showCounter: number=0;

  changeCounter(image: Image): void{
    this.galleryService.changeVisibility(image);
    this.showCounter=this.galleryService.countVisibility();
  }
}


