import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { GalleryitemComponent } from './components/galleryitem/galleryitem.component';

@NgModule({
  declarations: [
    AppComponent,
    GalleryitemComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
