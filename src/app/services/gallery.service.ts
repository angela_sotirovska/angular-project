import { Injectable } from '@angular/core';
import { Image } from '../image';

@Injectable()
export class GalleryService {

  images: Image[] = [
    {url: "http://nextgeneration.com.mk/wp-content/uploads/2020/06/Instagram-logo.jpg", 
    description: "This is insta logo!", visibility:  false},
    {url: "https://www.pngkit.com/png/detail/637-6371563_facebook-small-icon-png.png", 
    description: "This is facebook logo!", visibility: false},
    {url: "https://www.vectorico.com/download/social_media/Whatsapp-Icon.jpg", 
    description: "This is whatsup logo!", visibility: false}
  ]

  constructor() { }

  getImages(): Image[]{
    return this.images;
  }

  changeVisibility(image: Image): void{
    image.visibility=!image.visibility;
  }

  countVisibility():number{
    let counter: number = 0;
    this.images.forEach(function(image){
      if(image.visibility){
        counter++;
      }
    })
    return counter;
  }
}
