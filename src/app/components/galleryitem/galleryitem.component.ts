import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Image } from 'src/app/image';
import { GalleryService } from 'src/app/services/gallery.service';

@Component({
  selector: 'app-galleryitem',
  templateUrl: './galleryitem.component.html',
  styleUrls: ['./galleryitem.component.scss']
})

export class GalleryitemComponent {

  @Input()
  img: Image;

  @Output() 
  counterClicks : EventEmitter<Image> = new EventEmitter();
  
  constructor(){}

  clickedImage(){
    this.counterClicks.emit(this.img);
  }
}
